let vari = "variable";
let num = "numero";
let comparisons = ["Mayor que", "Menor que", "Diferente de", "Es igual a", "Mayor o igual que", "Menor o igual que"];
let asignacion = "Asignación";
let term = "Terminal";
let exp = [];

function getInstruction(exp) {
    const instructions = [[]];
    let current = 0;
    for (let i = 0; i < exp.length; i++) {
        instructions[current].push(exp[i]);
        if (exp[i] === term) {
            current++;
            i !== exp.length - 1 && instructions.push([]);
        }
    }
    return instructions;
}
exp = getInstruction(exp)
function getSintactic(exp) {

    return exp.map((el) => {
        exp.forEach(el => el[el.length-1] !== term);
        exp.forEach(el => el.includes("error"));
        if(!el.includes("error")){
            return instruction(el),true;  
        }
        else{
           return false;
        }
    });
}
function instruction(instruction) {
    let result;
    if(instruction.length === 2){
        result = termi(instruction[0]);
    }
    else{
        result = expression(instruction.slice(0, -1));
    }
    return result;
}
function termi(term) {
    const result = number(term[0]) || variable(term[0]);
    return result;
}
function expression(expression) {
    let result2;
    if(expression[1] === asignacion){
        result2 = asignation(expression);
    }
    else{
        result2 = comparison(expression);
    }
    return result2;
}
function asignation(asignation) {
    let result3;
    if(variable(asignation[0]) && asign(asignation[1])){
        result3 = secondTerm(asignation.slice(2, asignation.length));
    }
    else{
        result3 = false;
    }
    return result3;
}
function secondTerm(secondTerm) {
    let result4;
    if(secondTerm.length === 1){
        result4 = termi(secondTerm);
    }
    else{
        result4 = comparison(secondTerm);
    }
    return result4;
}
function comparison(comparison) {
    const result = (variable(comparison[0]) || number(comparison[0])) && comparation(comparison[1]) && (variable(comparison[2]) || number(comparison[2]));
    if (comparison.length > 3)
        return result && comparison(comparison.slice(2, comparison.length));
    return result;
}
function variable(variable) {
    let result5;
    if(variable === vari){
        reuslt5 = variable;
    }
    return result5;
}
function number(number) {
    const result6 = number === num;
    return result6;
}
function comparation(operator) {
    const result7 = comparisons.includes(operator);
    return result7;
}
function asign(asign) {
    const result8 = asign === asignacion;
    return result8;
}


module.exports = {
    "getSintactic": getSintactic,
    "getInstruction": getInstruction
};
