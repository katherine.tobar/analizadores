let analysis = [];
let characters = new RegExp("^[a-zA-Z]+$");
let numbers = new RegExp("^[0-9]+$");
let current = 0;
analysis.length = 0;

function getLexico(expression){
    for(let i=0;i<expression.length;i++){
        if (expression[i] === "=") {
            if(analysis[current]){
                analysis[current] = analysis[current].concat(expression[i]);
            }else{
                analysis[current] = expression[i];
            }
            expression[i + 1] !== "=" && current++;
        } else if (expression[i] === ">" || expression[i] === "<") {
            analysis[current] = expression[i];
            expression[i + 1] !== "=" && current++;
        } else if(expression[i] === "!") {
            analysis[current] = expression[i];
            expression[i + 1] !== "=" && current++;
        } else if(expression[i] === ";") {
            analysis[current] = expression[i];
            current++;
        }
        else if (numbers.test(expression[i])) {
            if(analysis[current]){
                analysis[current] = analysis[current].concat(expression[i]);
            }else{
                analysis[current] = expression[i];
            }
            !(numbers.test(expression[i + 1])) && current++;
        }
        else if (characters.test(expression[i])) {
            if(analysis[current]){
                analysis[current] = analysis[current].concat(expression[i]);
            }else{
                analysis[current] = expression[i];
            }
            if (!(characters.test(expression[i + 1])) && !(numbers.test(expression[i + 1]))) {
                current++;
            }
        }    
    }
    current = 0;
    return analysis.map
        (character => {
            if(characters.test(character)){
                return `variable`;
            }
            else if(numbers.test(character)){
                return `numero`;
            } 
            else if(character === "=="){
                return "Es igual a";
            }
            else if(character === "!=" ){
                return "Diferente de";
            }
            else if(character === "="){
                return "Asignación";
            }
            else if( character === ">"){
                return "Mayor que";
            }
            else if(character === ">="){
                return "Mayor o igual que";
            }
            else if( character === "<"){
                return "Menor que";
            }
            else if(character === "<="){
                return "Menor o igual que";
            }
            else if(character === "("){
                return "Inicio de paréntesis";
            } 
            else if(character === ")"){
                return "Cierre de paréntesis";
            }
            else if(character === ";"){
                return "Terminal";
            } 
            else if(character === "!"){
                return "error";
            }
        });             
}

module.exports = {
    "getLexico": getLexico
}