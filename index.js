const analizadorLexico = require('./src/lexico.js');
const analizadorSintactico = require('./src/sintactico.js');

var expression = "x = 1 < 2; x != 6; y = 12; y>x;";
var answer;
var answerSint;

answer = analizadorLexico.getLexico(expression);

console.log("¡ANÁLISIS LÉXICO!");
console.log("Expresión analizada: '" +expression+"'");
console.log(answer);

var separated = analizadorSintactico.getInstruction(expression);
answer = analizadorSintactico.getSintactic(separated);

console.log("¡ANÁLISIS SINTÁCTICO!");
console.log(separated);
console.log(answer);